// The Vue build version to load with the `import` command
// (runtime-only or standalone) has been set in webpack.base.conf with an alias.
import Vue from 'vue'
import App from './App'
import router from './router'
import ElementUI from 'element-ui'; //加入
import 'element-ui/lib/theme-chalk/index.css';//加入
import Video from 'video.js'
import 'video.js/dist/video-js.css'
import axios from 'axios'
import VueAxios from 'vue-axios'
import qs from 'qs';

Vue.use(qs)

Vue.use(ElementUI)


Vue.use(VueAxios,axios)

Vue.prototype.$video = Video

Vue.config.productionTip = false
Vue.use(ElementUI)

/* eslint-disable no-new */
new Vue({
  el: '#app',
  router,
  components: { App },
  template: '<App/>'
})
