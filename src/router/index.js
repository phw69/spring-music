import Vue from 'vue'
import Router from 'vue-router'
import Home from '@/pages/home.vue'
import player from '@/pages/player.vue'
import Play from '@/components/music-audio-play.vue'
import Like from "../components/mypages/Like"
import song from "../components/mypages/LikeList/song";
import create from "../components/mypages/create";
import follow from "../components/mypages/follow";
import siger from "../components/mypages/siger";
import paylist from "../components/mypages/LikeList/paylist";
import myplayer from "../pages/myplayer";
import login from "../components/mypages/login";
import MyVideo from "../components/mypages/LikeList/MyVideo";
import register from "../components/register";
Vue.use(Router)

export default new Router({
  routes: [{
    path: '/',
    name: 'home',
    component:Home
  },{
    path:'/player',
    name:'player',
    component:player
  },
  {
    path:'/play',
    name:'music-audio-play',
    component:Play
  },{
    path:"/Like",
    component:Like
  }
  ,{
    path:"/song",
    component:song
  }
    ,{
      path:"/create",
      component:create
    },{
    path:"/follow",
      component:follow
    },{
    path:"/siger",
      component:siger
    },
    {
      path:"/paylist",
      component:paylist
    },
    {
      path:"/myplayer",
      component:myplayer
    },{
    path:'/login',
      component:login
    },{
    path:'/myvideo',
      component:MyVideo
    },{
      path:'/register',
      component:register
    }
  ]
})
